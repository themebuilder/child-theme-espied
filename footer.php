<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Espied
 */

if(is_active_sidebar('footer-sidebar-1')){
  ?>
  <div id="footer-sidebar" class="secondary">
    <div id="footer-sidebar1">
    <?php
      dynamic_sidebar('footer-sidebar-1');
    ?>
    </div>
  <?php
  if(is_active_sidebar('footer-sidebar-2')){
  ?>
    <div id="footer-sidebar2">
    <?php
      dynamic_sidebar('footer-sidebar-2');
    ?>
    </div>
  <?php
  }
  if(is_active_sidebar('footer-sidebar-3')){
  ?>
    <div id="footer-sidebar3">
    <?php
      dynamic_sidebar('footer-sidebar-3');
    ?>
    </div>
  <?php
  }
  if(is_active_sidebar('footer-sidebar-4')){
  ?>
    <div id="footer-sidebar3">
    <?php
      dynamic_sidebar('footer-sidebar-4');
    ?>
    </div>
  <?php
  }
  ?>
  </div>
  <?php
}
?>

<?php wp_footer(); ?>

</body>
</html>
