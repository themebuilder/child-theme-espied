<?php
	 add_action( 'wp_enqueue_scripts', 'child_theme_espied_enqueue_styles' );
	 function child_theme_espied_enqueue_styles() {
		 	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
			wp_dequeue_style( 'parent-style');
 		  }

			/**
			 * Register three widget area to include in footer.php
			 * @see /footer.php
			*/

			function child_theme_espied_widgets_init() {
			register_sidebar( array(
			'name' => 'Footer Sidebar 1',
			'id' => 'footer-sidebar-1',
			'description' => 'Appears in the footer area',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
			) );
			register_sidebar( array(
			'name' => 'Footer Sidebar 2',
			'id' => 'footer-sidebar-2',
			'description' => 'Appears in the footer area',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
			) );
			register_sidebar( array(
			'name' => 'Footer Sidebar 3',
			'id' => 'footer-sidebar-3',
			'description' => 'Appears in the footer area',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
			) );
			}
			add_action( 'widgets_init', 'child_theme_espied_widgets_init' );

			// Adding Google Analytics Snippet
			add_action( 'wp_head', function() { ?>
			    <!-- Global site tag (gtag.js) - Google Analytics -->
					<script async src = " https://www.googletagmanager.com/gtag/js?id=UA-128340428-1 "></script>
					<script>
						window.dataLayer = window.dataLayer || [];
						function gtag () {dataLayer.push (arguments);}
						gtag ('js', new Date ());
						gtag ('config', 'UA-128340428-1');
						</script>
			<?php } );
?>
